# Basket



## Routes


## Рассчитанная корзина
```POST /basket/calculated```



Пример тела запроса:

```json
{
    "products" : [
        {
            "productId" : "1",
            "productCount" : "2"
        },
        {
            "productId" : "2",
            "productCount" : "4"
        }
    ],
    "paymentType" : "CARD"
}
```

Пример ответа:

```json
{
  "products": [
    {
      "productId": 1,
      "productCount": 2,
      "price": 100
    },
    {
      "productId": 2,
      "productCount": 4,
      "price": 100
    }
  ],
  "totalPrice": 200
}
```

##Что можно улучшить

Изменены типы на Long и BigDecimal.

Тип id продукта можно поменять на UUID.

Добавить сущности и маппер.

Написать тесты.



