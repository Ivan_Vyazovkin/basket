package com.basket.basket.service;

import com.basket.basket.dto.BasketRequest;
import com.basket.basket.dto.BasketResponse;

public interface BasketService {
    BasketResponse calculated(BasketRequest basketRequest);
}
