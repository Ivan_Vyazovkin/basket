package com.basket.basket.service.impl;

import com.basket.basket.dto.BasketRequest;
import com.basket.basket.dto.BasketResponse;
import com.basket.basket.dto.ProductResponse;
import com.basket.basket.service.BasketService;
import com.basket.basket.service.PriceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class BasketServiceImpl implements BasketService {
    private final PriceService priceService;

    @Override
    public BasketResponse calculated(BasketRequest basketRequest) {
        List<ProductResponse> products = basketRequest.getProducts().stream()
                .map(priceService::getPriceProducts
                ).collect(Collectors.toList());
        return new BasketResponse(
                products,
                products.stream()
                        .map(ProductResponse::getPrice)
                        .reduce(BigDecimal.ZERO, BigDecimal::add)
        );
    }
}
