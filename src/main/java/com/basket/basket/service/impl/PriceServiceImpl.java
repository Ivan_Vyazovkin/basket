package com.basket.basket.service.impl;

import com.basket.basket.dto.ProductRequest;
import com.basket.basket.dto.ProductResponse;
import com.basket.basket.exception.ConnectException;
import com.basket.basket.service.PriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Service
public class PriceServiceImpl implements PriceService {
    private final HttpHeaders headers;
    private final RestTemplate restTemplate;

    @Override
    @Cacheable(cacheNames = "priceProduct", key = "#productRequest.productId")
    public ProductResponse getPriceProducts(ProductRequest productRequest) {
        try {
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<ProductRequest> requestEntity = new HttpEntity<>(productRequest, headers);
            ResponseEntity<ProductResponse> responseEntity = restTemplate.postForEntity(
                    "http://localhost:8080/products/price/update",
                    requestEntity,
                    ProductResponse.class
            );
            return new ProductResponse(
                    responseEntity.getBody().getProductId(),
                    responseEntity.getBody().getProductCount(),
                    responseEntity.getBody().getPrice()
            );
        } catch (RuntimeException e) {
            throw new ConnectException("Price service not found");
        }
    }
}
