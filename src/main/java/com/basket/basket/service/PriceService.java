package com.basket.basket.service;

import com.basket.basket.dto.ProductRequest;
import com.basket.basket.dto.ProductResponse;

public interface PriceService {
    ProductResponse getPriceProducts(ProductRequest productRequest);
}
