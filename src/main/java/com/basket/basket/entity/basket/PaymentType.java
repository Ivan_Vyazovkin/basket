package com.basket.basket.entity.basket;

public enum PaymentType {
    CARD,
    CASH,
    PAYPAL
}
