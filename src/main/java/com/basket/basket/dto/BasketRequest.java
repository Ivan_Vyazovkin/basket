package com.basket.basket.dto;

import com.basket.basket.entity.basket.PaymentType;
import lombok.*;
import org.springframework.lang.Nullable;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BasketRequest {
    private List<ProductRequest> products;
    private PaymentType paymentType;
    @Nullable
    private Long addressId;
}
