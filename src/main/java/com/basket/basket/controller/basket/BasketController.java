package com.basket.basket.controller.basket;

import com.basket.basket.dto.BasketRequest;
import com.basket.basket.dto.BasketResponse;
import com.basket.basket.service.BasketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/basket")
public class BasketController {
    private final BasketService basketService;

    @PostMapping("/calculated")
    public BasketResponse calculated(@RequestBody BasketRequest basketRequest) {
        return basketService.calculated(basketRequest);
    }
}
