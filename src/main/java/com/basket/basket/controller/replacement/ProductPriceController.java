package com.basket.basket.controller.replacement;

import com.basket.basket.dto.ProductRequest;
import com.basket.basket.dto.ProductResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductPriceController {

    @PostMapping("/price/update")
    public ProductResponse getPriceProducts(@RequestBody ProductRequest productRequest) {
        return  new ProductResponse(
                    productRequest.getProductId(),
                    productRequest.getProductCount(),
                        new BigDecimal(100)
                );
    }
}
